#!/usr/bin/env bash
# Installs s2tagger.

# Prerequisites:
#   - git
#   - python3 >= 3.7

set -e

DEPSDIR=$1

echo Installing s2tagger
python3 -m venv ${DEPSDIR}/venv
source ${DEPSDIR}/venv/bin/activate
pip install poetry

git clone https://gitlab.com/nats/s2tagger.git ${DEPSDIR}/s2tagger
cd ${DEPSDIR}/s2tagger
git checkout v1.0.1
poetry install
