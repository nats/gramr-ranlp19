#!/usr/bin/env python3

import sys, re

if(len(sys.argv) != 3):
    print("Usage: alignments_to_attr.py <amr corpus file> <alignment file>")
    exit(1)

amrFileName = sys.argv[1]
alignmentFileName = sys.argv[2]

alignmentFile = open(alignmentFileName)
alignmentLines = alignmentFile.readlines()
alignmentFile.close()

# Add the attributes to the AMR file
amrFile = open(amrFileName)

currentAlignmentLine = 0
line = amrFile.readline()
while line:
    if line.startswith('# ::id'):
        print(line, end='')
        print('# ::alignments-isi {0}'.format(alignmentLines[currentAlignmentLine]), end='')
        print()
        currentAlignmentLine += 1
    line = amrFile.readline()

amrFile.close()
