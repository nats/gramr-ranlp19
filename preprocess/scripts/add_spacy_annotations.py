#!/usr/bin/env python3

import spacy
import sys

nlp = spacy.load('en')

if(len(sys.argv) != 2):
    print('Usage: add_spacy_annotations.py <amr corpus file>')

amrFileName = sys.argv[1]

amrFile = open(amrFileName)

def show_span(span):
    return '{},{},{}'.format(span.start, span.end, span.label_)

def show_arc(t):
    return '{},{},{}'.format(t.head.i, t.i, t.dep_)

def print_attrs(snt):
    doc = nlp(snt)
    
    # print tokens
    print('# ::tok {}'.format(' '.join(t.text for t in doc)))

    # print lemmas
    print('# ::lemmas {}'.format(' '.join(t.lemma_ for t in doc)))

    # print ner tags
    print('# ::ent {}'.format(' '.join(show_span(span) for span in doc.ents)))

    # print dependency edges
    print('# ::dep {}'.format(' '.join(show_arc(t) for t in doc)))
    

line = amrFile.readline()
wasInComment = False
snt = None
while line:
    if line.startswith("# ::"):
        wasInComment = True
        if line.startswith("# ::snt"):
            snt = line[8:].strip()
    else:
        if wasInComment:
            # We're at the end of the comment section now
            print_attrs(snt)
        wasInComment = False
    print(line, end="")
    line = amrFile.readline()

amrFile.close()
