#!/usr/bin/env bash
set -e

base=$1

for file in $(ls $base/training)
do
  echo training $file
done
