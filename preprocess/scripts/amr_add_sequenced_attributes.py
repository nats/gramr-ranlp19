#!/usr/bin/env python3

import sys, re

if(len(sys.argv) != 2):
    print("Usage: amr_add_sequenced_attributes.py <attribute file> < <amr corpus file>")
    exit(1)

attrFileName = sys.argv[1]

attrFile = open(attrFileName)
attrLines = [line.strip() for line in attrFile.readlines() if len(line.strip()) > 0]
attrFile.close()

values = []

def print_attr(idx):
    print(attrLines[idx])

idx = 0
line = sys.stdin.readline()
wasInComment = True
while line:
    if line.startswith("#"):
        wasInComment = True
    elif line.strip():
        if wasInComment:
            # Insert the attributes at the end of the comments section
            print_attr(idx)
            idx += 1
        wasInComment = False
    print(line, end="")
    line = sys.stdin.readline()
