scripts=`dirname $0`
infile=$1
outfile=$2
anno=$3
work=$4

for file in `find ${CORENLP}/ -name "*.jar"`; do export CLASSPATH="$CLASSPATH:`realpath $file`"; done

$scripts/amr_to_raw.py snt "$infile" > "$work/raw"
java -Xmx4g edu.stanford.nlp.pipeline.StanfordCoreNLP -annotators $anno -ssplit.eolonly true -file "$work/raw" -outputFormat conll -outputDirectory "$work"
$scripts/add_corenlp_annotations.py "$infile" "$work/raw.conll" > "$outfile"
