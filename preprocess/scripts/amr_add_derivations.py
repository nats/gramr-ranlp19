#!/usr/bin/env python3

import sys, re

if(len(sys.argv) != 3):
    print("Usage: amr_add_derivations.py <amr corpus file> <auto file>")
    exit(1)

amrFileName = sys.argv[1]
autoFileName = sys.argv[2]

autoFile = open(autoFileName)
autoLines = autoFile.readlines()
autoFile.close()

# Collect all the attributes and derivations and make them accessible by
# sentence ID.
attributes = {}

def add_entry(attrDict):
    key = attrDict["ID"]
    if key not in attributes:
        attributes[key] = []
    attributes[key] = attributes[key] + [attrDict]

def print_attr(id):
    key = str(id)
    if key in attributes:
        n = 0
        for attrDict in attributes[key]:
            print("# ::ccg-derivation-{0} {1}".format(n, attrDict["derivation"]))
            n += 1

for i in range(0, len(autoLines), 2):
    (infoLine, derLine) = autoLines[i:i+2]
    attrDict = {}
    for attr in infoLine.split():
        (name, value) = attr.split("=")
        attrDict[name] = value
    attrDict["derivation"] = derLine.strip()
    add_entry(attrDict)

# Add the attributes to the AMR file
amrFile = open(amrFileName)

itemId = 1
line = amrFile.readline()
wasInComment = False

while line:
    if line.startswith("# ::"):
        wasInComment = True
    else:
        if wasInComment:
            # We're at the end of the comment section now
            print_attr(itemId)
            itemId += 1
        wasInComment = False
    print(line, end="")
    line = amrFile.readline()

amrFile.close()
        
