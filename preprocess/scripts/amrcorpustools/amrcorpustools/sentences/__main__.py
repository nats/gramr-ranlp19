import sys
from argparse import ArgumentParser

from amrcorpustools.example import parse_corpus


def main():
    argparser = ArgumentParser(
        usage='Extracts the sentences from an AMR corpus, one sentence per line.')
    argparser.add_argument('--input', '-i', required=False, help='The AMR corpus to process')
    argparser.add_argument('--attribute', '-a', default='snt', help='The attribute to extract sentences from')

    args = argparser.parse_args()

    input_file = open(args.input) if args.input else sys.stdin
    input_examples = parse_corpus(input_file.readlines())

    for example in input_examples:
        print(example.attributes[args.attribute][0])


if __name__ == '__main__':
    main()
