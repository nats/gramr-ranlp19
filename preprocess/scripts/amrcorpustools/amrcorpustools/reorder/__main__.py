import sys
from argparse import ArgumentParser

from amrcorpustools.example import parse_corpus, write_corpus


def main():
    argparser = ArgumentParser(
        usage='Changes the order of examples in a corpus so that it matches the order in another given corpus file. '
              'Useful if you’ve received unordered output from a parser and want to compare it to another file.')
    argparser.add_argument('--input', '-i', required=False, help='The AMR corpus to reorder')
    argparser.add_argument('--by', required=True, help='The AMR corpus whose example order to apply to the input')

    args = argparser.parse_args()

    input_file = open(args.input) if args.input else sys.stdin
    by_file = open(args.by)
    output_file = sys.stdout

    reorder(input_file, by_file, output_file)


def reorder(input_file, by_file, output_file):
    input_examples = parse_corpus(input_file.readlines())
    by_examples = parse_corpus(by_file.readlines())

    input_examples_by_id = {e.attributes['id'][0]: e for e in input_examples}
    by_ids = [e.attributes['id'][0] for e in by_examples]
    input_examples_reordered = [input_examples_by_id[i] for i in by_ids]

    write_corpus(input_examples_reordered, output_file)


if __name__ == '__main__':
    main()
