function annotate_spacy {
  infile=$1
  outfile=$2
  "$scripts/add_spacy_annotations.py" "$infile" > "$outfile"
}

function annotate_corenlp {
  echo Running CoreNLP pipeline on $1
  "$scripts/annotate_corenlp.sh" "$1" "$2" tokenize,ssplit,pos,lemma,ner "$work"
}

function align_jamr {
  infile=$1
  outfile=$2
  echo Adding jamr alignments to $1
  . "$JAMR/scripts/config.sh"
  java -cp "$JAMR/target/scala-2.10/jamr-assembly-0.1-SNAPSHOT.jar" edu.cmu.lti.nlp.amr.Aligner -v 0 < "$infile" | sed 's/::alignments /::alignments-jamr /' > "$outfile"

}

function align_isi {
  infile=$1
  outfile=$2
  echo Adding isi alignments to $1
  "$scripts/amr_add_attribute.py" work/isi_attr alignments-isi < "$infile" > "$outfile"
}

function align_tamr {
  infile=$1
  outfile=$2
  echo Adding tamr alignments to $1
  "$scripts/amr_add_attribute.py" work/tamr_alignment_converted alignments-tamr < "$infile" > "$outfile"
}

function supertag_easyccg {
  infile=$1
  outfile=$2
  echo Running EasyCCG tagger on $1
  "$scripts/amr_to_raw.py" tok "$infile" > "$work/raw.tok"
  "$scripts/add_easyccg_tags.sc" "$work/raw.tok" > "$work/easyccg_tags"
  "$scripts/amr_add_sequenced_attributes.py" "$work/easyccg_tags" < "$infile" > "$outfile"
}

function parse_easyccg {
  infile=$1
  outfile=$2
  echo Running EasyCCG parser on $1
  "$scripts/amr_to_raw.py" tok "$infile" > "$work/raw.tok"
  java -Xmx1500m -jar "$EASYCCG/easyccg.jar" -m "$EASYCCG/model_rebank" -f "$work/raw.tok" -l 50 -i tokenized --supertaggerbeam 0.0001 --nbest 10 > "$work/auto" 2>/dev/null
  "$scripts/amr_add_derivations.py" "$infile" "$work/auto" > "$outfile"
}
