#!/usr/bin/env python3

import sys, re

if len(sys.argv) != 3:
    print("Usage: amr_to_raw.py <attribute name> <amr corpus file>")
    exit(1)

attributeName = sys.argv[1]
amrFileName = sys.argv[2]

amrFile = open(amrFileName)

line = amrFile.readline()
prefix = '# ::{0}'.format(attributeName)
while line:
    if line.startswith(prefix):
        toks = line[len(prefix) + 1:]
        # Remove leading # to stop easyccg from thinking it’s a comment
        while toks.startswith('#'):
            toks = toks[1:]
        print(toks, end='')
    line = amrFile.readline()

amrFile.close()
