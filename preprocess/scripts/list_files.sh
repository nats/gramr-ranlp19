#!/usr/bin/env bash
set -e

base=$1

for dir in $(ls $base)
do
  for file in $(ls $base/$dir)
  do
    echo $dir $file
  done
done
