#!/usr/bin/env bash
set -e

# Extract ELMo vectors

ELMO_DATA=${DATA}/resources/elmo
mkdir -p ${ELMO_DATA}
cd ${ELMO_DATA}

splits=( training dev test )
for split in "${splits[@]}"; do
    python3 -m amrcorpustools.sentences --attribute tok < ${DATA}/processed/${split}/amr-release-1.0-${split}-proxy.txt > ${split}-proxy-sentences.txt
    python3 -m amrcorpustools.sentences --attribute id < ${DATA}/processed/${split}/amr-release-1.0-${split}-proxy.txt > ${split}-proxy-ids.txt

    allennlp elmo ${split}-proxy-sentences.txt ${split}-proxy-elmo.hdf5 --all
done
