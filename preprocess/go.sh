#!/usr/bin/env bash
# Process the AMR corpus.
#
# Usage: go.sh <CORPUS>
#     where corpus is the location of the unpacked AMR 1.0 corpus.
#
# Annotated corpus files are written to the 'processed' directory.
#
# Expected environment variables:
#
#    CORENLP
#        should point to a directory where Stanford CoreNLP is installed. Example:
#        /some/path/stanford-corenlp-full-2018-10-05
#
#    ISI_ALIGNER
#        should point to a directory where the ISI aligner is installed.
#
#    MGIZA
#        should point to a directory where mgiza is installed
#
# Prerequisites:
#
#     python3
#     parallel
#     amm (Ammonite)

set -e

SCRIPT_PATH="`dirname \"$0\"`"

CORPUS=$1

${SCRIPT_PATH}/scripts/process.sh "$CORPUS/data/split" processed
