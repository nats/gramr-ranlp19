#!/usr/bin/env bash
# Installs tools necessary for extracting elmo vectors.

# Prerequisites:
#   - git

set -e

DEPSDIR=$1

echo Installing allennlp
python3 -m venv ${DEPSDIR}/venv
source ${DEPSDIR}/venv/bin/activate
pip install allennlp

echo Installing amrcorpustools
pip3 install -e preprocess/scripts/amrcorpustools
