# gramr Experiments from RANLP19

This project reproduces the experiments in “Exploring Graph-Algebraic CCG
Combinators for Syntactic-Semantic AMR Parsing” (Beschke, RANLP19). The
remainder of this README explains how to run the experiments.

Despite efforts to keep things as simple as possible, running the experiments
entails a number of steps, which are described in more detail below.

1. **Prerequisites.** Obtain the AMR corpus and set up your local computing
   environment to be able to follow the remaining steps.
2. **Preprocessing.** Uses external tools to enrich the AMR corpus with
   additional attributes. This step requires the use of Docker.
3. **Experiments.** The experiment pipeline is divided into the steps
   **induce**, **tag**, and **parse**. You can choose to run these steps with
   Docker (easy setup) or without (full control, no root required).

**WIP Note:** This project is concerned with setting up the experiments to run
with docker for easy replicability. Unfortunately, I did not manage to get
everything running before RANLP19. I will set up the remaining steps by October
2019.

The following repositories contain the actual source code used in the
experiments:

- [gramr](https://gitlab.com/nats/gramr) Semantic Parser
- [s2tagger](https://gitlab.com/nats/s2tagger) Supertagger

Don't hesitate to contact me at `beschke@informatik.uni-hamburg.de` if you need
help with anything.

## Prerequisites

You will need the following to run the experiments using docker:

- A computer you can run Docker on – e.g. a Linux box where you have root access.
- [Docker](https://docker.com) installed on your computer. On Ubuntu 18.04, you can use

     ```bash
     $ sudo apt-get install docker.io docker-compose
     ```
    
  to install it. Check the [Docker website](https://docker.com/get-started) for
  installation options on other systems.
- [The AMR 1.0 (LDC2014T12) data set](https://catalog.ldc.upenn.edu/LDC2014T12).

## Preprocessing

Preprocessing uses a diverse set of tools, which are pulled from a wide variety
of sources and which in turn require very specific dependencies. In order to
account for all these issues, the experimental configuration is described here
as a `Dockerfile`.

Here, we show how preprocessing can be run using the docker image. If you want
to avoid Docker, the `Dockerfile` should give you hints on how to set everything
up.

### Unpacking the Data

Before you begin, you must unpack the AMR corpus into the `data` directory. This
directory is mounted by docker and used to access the corpus data. E.g.:

```bash
$ cd data/
$ cp ~/Downloads/amr_anno_1.0_LDC2014T12.tgz .
$ tar -xzf amr_anno_1.0_LDC2014T12
$ ls
amr_anno_1.0
```

### Obtaining the Docker Image

The easiest way to get started with preprocessing is to download the pre-built
Docker image from Dockerhub. You can do so by issuing the following command:

```bash
$ docker pull xian9426/gramr-ranlp19:latest
```

Alternatively, you can build the image yourself locally (which will download
even more data and take a long time). See the *Building the Docker Image*
section below.

### Running the Preprocessing Script

The `docker-compose.yaml` file sets up a container to run the experiments. It
includes a command that performs the preprocessing.

```bash
$ docker-compose run --rm gramr-ranlp19 preprocess
```

## Experiments

You can run the experiments with or without Docker. While Docker is the easiest
option, it requires administrative access and you may want to run experiments on
machines you don't own (e.g. the powerful cluster blades offered by your
university). Especially parsing really benefits from having lots of cores and
RAM.

### Running Experiments With Docker

The included docker-compose file sets up a container to run the experiments. It
allows you to choose one of the following actions:

- `induce`
- `tag`
- `parse` **WIP**

The actions must be run in this order. The general syntax for executing an action is as follows:

```bash
$ docker-compose run --rm gramr-ranlp19 <action>
```

**NOTE:** Unfortunately, not the whole pipeline is set up with Docker so far.
Please check back in October 2019 for the `parse` command.

### Running Experiments Without Docker

#### Prerequisites

To run experiments without Docker, you must first make sure your system fulfills
the basic requirements.

- Python 3.7 or higher is required:

  ```bash
  $ python3 -v
  3.7.4
  ```

- [sbt](https://www.scala-sbt.org/) and `git` must be available

#### Installing

Run the install script to install `gramr` and `s2tagger`.

```bash
$./install.sh
```

#### Running

Use the `run.sh` script to run experiments with the same actions as above:

```bash
$ ./run.sh <action>
```

where `<action>` is one of `induce`, `tag`, `train-parser`, `test-parser`. Or
simply omit `<action>` to run all of them.

## Building the Docker Image

### Starting the build

```bash
$ docker-compose build
```

### Optional: Building with a squid proxy

This build pulls in several gigabytes of dependencies. If you plan on tinkering
with the Dockerfile, you're encouraged to use a squid proxy to speed up repeated
builds. If you have squid running on your local machine, you can use the provided
docker-compose file:

```bash
$ docker-compose -f squid/docker-compose.yaml build
```

To use squid, first install it on your local machine. E.g., on Ubuntu 18.04:

```bash
$ sudo apt install squid
```

A squid config file is also included for reference. The following options turned
out to be important. Set them in `/etc/squid/squid.conf`:

```
acl localhost src 127.0.0.1                  # allows access from localhost
maximum_object_size 1024 MB                  # enables caching of large files
cache_dir aufs /var/spool/squid 5000 16 256  # enables disk cache
```
