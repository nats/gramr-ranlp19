FROM ubuntu:18.04

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# Install dependencies.

# Add deb source for sbt
RUN apt-get update && apt-get install -y gnupg ca-certificates \
  && echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list \
  && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823

# Both python2.7 and python3.6 are needed:
# python2.7 is needed for the ISI aligner.
# python3.6 is needed for our own scripts.
RUN apt-get update \
  && apt-get install -y \
       build-essential cmake \
       libboost-dev libboost-thread-dev libboost-system-dev \
       git curl wget unzip bzip2 \
       python2.7 python-pip \
       python3.6 python3-pip python3-venv \
       openjdk-8-jdk \
       sbt

# Install poetry (needed for Python dependency management)
RUN pip3 install poetry==0.12.17

# Install ammonite (needed for Scala scripts)
RUN sh -c '(echo "#!/usr/bin/env sh" && curl -L http://github.com/lihaoyi/Ammonite/releases/download/1.6.9/2.13-1.6.9) > /bin/amm && chmod +x /bin/amm'

# Install CoreNLP
RUN wget -q http://nlp.stanford.edu/software/stanford-corenlp-full-2018-10-05.zip \
  && unzip stanford-corenlp-full-2018-10-05.zip \
  && mv /stanford-corenlp-full-2018-10-05 /corenlp

# Install ISI aligner
COPY isialign_patch /isialign_patch
RUN wget -q https://isi.edu/~damghani/papers/Aligner.zip \
  && unzip Aligner.zip \
  && mv /Publish_Version /isialigner \
  && cat /isialign_patch/filter-eng-by-stopwords.py.diff | patch /isialigner/scripts/filter-eng-by-stopwords.py

# Install mgiza
RUN git clone https://github.com/moses-smt/mgiza.git /mgiza
WORKDIR /mgiza/mgizapp
RUN git checkout 3dd8ef62a31a0fb5e57bc720760d3f0b3cbe6fe8
RUN cmake . && make && make install
WORKDIR /

# Install JAMR
RUN git clone https://github.com/jflanigan/jamr.git /jamr
WORKDIR /jamr
RUN git checkout ddfbe318dea39e746c5e6cf559d5a286cb0211f0
RUN ./setup && . scripts/config.sh && ./compile
WORKDIR /

# Install EasyCCG
RUN git clone https://github.com/mikelewis0/easyccg.git /easyccg
WORKDIR /easyccg
RUN git checkout e42d58e08eb2a86593d52f730c5afe222e939781
# The CCGbank-rebanked model has to be downloaded from Google Drive.
COPY bin/gdrive_download /bin/
RUN gdrive_download https://drive.google.com/open?id=0B7AY6PGZ8lc-cHVuUTVQVzdjdGM \
  && tar -xzf model_rebank.tar.gz
WORKDIR /

# Download TAMR alignments
RUN wget -q https://github.com/Oneplus/tamr/raw/master/release/ldc2014t12/amr-release-1.0-training_fix.txt.sd_tok.tamr_alignment.bz2 \
  && bunzip2 amr-release-1.0-training_fix.txt.sd_tok.tamr_alignment.bz2 \
  && mv amr-release-1.0-training_fix.txt.sd_tok.tamr_alignment /tamr_alignment

# Install AllenNLP (needed for ELMo)
RUN pip3 install allennlp==0.8.5

# Install gramr
RUN git clone https://gitlab.com/nats/gramr.git
WORKDIR /gramr
RUN git checkout v272
RUN sbt assembly \
  && mv /gramr/target/scala-2.12/gramr-assembly-272.jar /gramr.jar
WORKDIR /
ENV GRAMR=/gramr.jar

# Install s2tagger
RUN git clone https://gitlab.com/nats/s2tagger.git && cd /s2tagger && git checkout v1.0.1
WORKDIR /s2tagger
RUN poetry install
WORKDIR /
ENV TAGGER=/s2tagger


# Add own code

VOLUME ["/data"]
ENV DATA=/data
COPY bin /bin

# Copy preprocessing code
COPY preprocess /preprocess
# Install included amrcorpustools
RUN pip3 install -e /preprocess/scripts/amrcorpustools

# Copy experiment code
COPY experiments /experiments

ENTRYPOINT ["entry"]
