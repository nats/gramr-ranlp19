#!/usr/bin/env bash
# Runs one or several experiments.
#
# Usage:
#
#    run.sh [STEP1 [STEP2 …]]

export GRAMR=$PWD/deps/gramr/gramr.jar
export TAGGER=$PWD/deps/s2tagger
export DATA=$PWD/data

source deps/venv/bin/activate

if [[ $# -eq 0 ]]
then
    commands=(elmo induce tag train-parser test-parser)
else
    commands=$@
fi

for cmd in ${commands[@]}
do
    bin/${cmd}
done
