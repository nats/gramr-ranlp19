#!/usr/bin/env bash
# Performs the supertagging step of the pipeline.
# Trains the supertagger and then predicts tags on the train, dev and test sets.
set -e

EXPERIMENT=$1
IN_DIR=${PWD}/experiments/tag
OUT_DIR=${DATA}/tag
CONFIG=${IN_DIR}/config.yaml

cd "${TAGGER}"

# Train
echo ${EXPERIMENT} Training
IN_DIR=${PWD}/experiments/tag
poetry run python -m s2tagger train -c ${CONFIG} -m ${OUT_DIR}/output --val 0.1
# Predict val + test
echo ${EXPERIMENT} Predicting val
IN_DIR=${PWD}/experiments/tag
poetry run python -m s2tagger predict -c ${CONFIG} -m ${OUT_DIR}/output --data-set val -o ${OUT_DIR}/output/val_tags_pred.txt
IN_DIR=${PWD}/experiments/tag
poetry run python -m s2tagger predict -c ${CONFIG} -m ${OUT_DIR}/output --data-set test -o ${OUT_DIR}/output/test_tags_pred.txt
# Jackknife
echo ${EXPERIMENT} Jackknifing / predicting train
IN_DIR=${PWD}/experiments/tag
poetry run python -m s2tagger jackknife -c ${CONFIG} -o ${OUT_DIR}/output/train_tags_pred.txt --splits 4
