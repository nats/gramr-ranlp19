#!/usr/bin/env bash
set -e

DATAROOT=${DATA}/processed
TEST=$DATAROOT/test/amr-release-1.0-test-proxy.txt
LEX_DIR=${DATA}/lexicon
TAG_DIR=${DATA}/tag/output

OUTDIR=${DATA}/model
mkdir -p $OUTDIR

GRAMR_OPTS="--logDir ${OUTDIR} --lexDir ${LEX_DIR} --tagDir ${TAG_DIR} --supertagFile test_tags_pred.txt --model ${OUTDIR}/4-model.ser --beamSize 15 --parallelism 16"
java -Xmx48g -cp $GRAMR gramr.experiments.RunScript experiments/scripts/test.sc --test $TEST $GRAMR_OPTS | tee gramr_test.log
