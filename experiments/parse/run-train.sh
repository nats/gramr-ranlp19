#!/usr/bin/env bash
set -e

DATAROOT=${DATA}/processed
TRAIN=${DATAROOT}/training/amr-release-1.0-training-proxy.txt
VAL=${DATAROOT}/dev/amr-release-1.0-dev-proxy.txt
LEX_DIR=${DATA}/lexicon
TAG_DIR=${DATA}/tag/output

OUTDIR=${DATA}/model
mkdir -p $OUTDIR

GRAMR_OPTS="--iterations 4 --logDir ${OUTDIR} --lexDir ${LEX_DIR} --tagDir ${TAG_DIR} --oracleBeamSize 20 --update early+cost --beamSize 15 --parallelism 16"
java -Xmx48g -cp $GRAMR gramr.experiments.RunScript experiments/scripts/trainOracle.sc --train $TRAIN --validate $VAL $GRAMR_OPTS | tee gramr_train.log
