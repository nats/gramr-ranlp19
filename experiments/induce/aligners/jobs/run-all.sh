#!/usr/bin/env bash
set -e

ALIGNMENTS=(jamr+isi jamr+tamr isi+tamr jamr+isi+tamr tamr isi jamr)

for ALIGNMENT in ${ALIGNMENTS[@]}; do
    ./run.sh ${ALIGNMENT}
done

