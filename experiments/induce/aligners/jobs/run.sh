#!/usr/bin/env bash
set -e
ALIGNMENT=$1

timeout 24h java -Xmx12g -jar ${GRAMR} ${SCRIPTS}/induce.sc --corpus ${DATA}/training/amr-release-1.0-training-proxy.txt --logDir "output/${ALIGNMENT}_\$DATE_\$TIME" --alignments ${ALIGNMENT} --maxUnalignedNodes 10 --maxItemCount 10000
