import gramr.ccg.EasyccgCombinatorSystem._
import gramr.construction.SyntacticContext
import gramr.construction.sgraph.SGraphRule
import gramr.graph.{AdjacencyMeaningGraph, Graph}
import gramr.graph.edit.sgraph.Operators

type MR = AdjacencyMeaningGraph
implicit val mgl: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance

// Define the CCG/ASKi rule set
def ccgAskiRules(maxModifieeDepth: Option[Int]): Vector[SGraphRule[MR]] =
  Vector(
    new SGraphRule[MR](
      "fa",
      (context: SyntacticContext) => context.combinator == FA,
      Operators.Application[MR](maxModifieeDepth = maxModifieeDepth),
      backward = false
    ),
    new SGraphRule[MR](
      "ba",
      (context: SyntacticContext) => context.combinator == BA,
      Operators.Application[MR](maxModifieeDepth = maxModifieeDepth),
      backward = true
    ),
    new SGraphRule[MR](
      "fc",
      (context: SyntacticContext) => context.combinator == FC,
      Operators.Application[MR](maxModifieeDepth = maxModifieeDepth),
      backward = false
    ),
    new SGraphRule[MR](
      "fc2",
      (context: SyntacticContext) => context.combinator == FC2,
      Operators.Application[MR](maxModifieeDepth = maxModifieeDepth),
      backward = false
    ),
    new SGraphRule[MR](
      "bxc",
      (context: SyntacticContext) => context.combinator == BXC,
      Operators.Application[MR](maxModifieeDepth = maxModifieeDepth),
      backward = true
    ),
    new SGraphRule[MR](
      "bxc2",
      (context: SyntacticContext) => context.combinator == BXC2,
      Operators.Application[MR](maxModifieeDepth = maxModifieeDepth),
      backward = true
    ),
    new SGraphRule[MR](
      "conj",
      (context: SyntacticContext) => context.combinator == Conj,
      Operators.Substitution[MR](),
      backward = false
    ),
    new SGraphRule[MR](
      "lp",
      (context: SyntacticContext) => context.combinator == LPunct,
      Operators.Identity[MR](),
      backward = false
    ),
    new SGraphRule[MR](
      "rp",
      (context: SyntacticContext) => context.combinator == RPunct,
      Operators.Identity[MR](),
      backward = true
    )
  )
