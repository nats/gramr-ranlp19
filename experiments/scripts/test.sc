import $file.commonSettings
import java.io.{BufferedWriter, File, FileWriter, PrintWriter}

import gramr.experiments._
import gramr.graph.AdjacencyMeaningGraph
import gramr.jobs.ops._
import gramr.learn._
import gramr.parse.Implicits._
import gramr.util.{Logging, ParseStatsCsvLogger}
import gramr.experiments.ScriptSupport._
import commonSettings._
import gramr.jobs
import gramr.learn.util.FeatureIndex

import scala.util.Random


@main
def main(
  test: String = "corpus/dev/amr-release-1.0-dev-proxy.txt",
  testSize: Option[Int] = None,
  rejectLength: Int = 40,
  beamSize: Int = 10,
  maxModifieeDepth: Int = 0,
  learner: String = "adadelta",
  model: String = "model.ser",
  parallelism: Option[Int] = None,
  lexDir: String = ".",
  tagDir: String = ".",
  supertagFile: String = "test_tags_pred.txt",
  logDir: String = "logs/test_$DATE_$TIME",
  seed: Int = 1
): Unit = {

  val logPath: File = Logging.setupLogging(logDir)

  implicit val random: Random = new Random(seed)

  val testExamples = loadTestData(test, tagDir, testSize, supertagFile=Some(supertagFile))


  // Build learner

  val learnerInstance = loadLearner(learner, model)
  implicit val featureIndex: FeatureIndex = learnerInstance.trainModel.featureIndex
  implicit val fv: FeatureVector[MapFeatureVector] = MapFeatureVector.featureVectorLike
  implicit val bv: BaseVector[MapFeatureVector] = fv


  // Load lexicon

  val lexicalChoice = loadLexicon[AdjacencyMeaningGraph](new File(lexDir), commonSettings.wildcardFillers)

  val actualParallelism = parallelism.getOrElse(  // default: allow at least 2GB per parsed example)
    Integer.min(
      Runtime.getRuntime.availableProcessors(),
      (Runtime.getRuntime.maxMemory() / (1024l * 1024l * 1024l * 2.0)).toInt))

  logger.info(s"Parsing with parallelism level $actualParallelism")


  // Build learned parser

  val combinatoryChoice = commonSettings.combinatoryChoice(maxModifieeDepth = maxModifieeDepth)

  val testDecoder = gramr.parse.postprocess.resolveCorefs(
    gramr.parse.cky.learned[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](
      lexicalChoice = lexicalChoice,
      binaryChoice = combinatoryChoice,
      combinatorSystem = simpleCs,
      decisionFeatures = featureSet.derivationFeatures.extract,
      stateFeatures = featureSet.stateFeatures.extract,
      modelScore = fv => learnerInstance.testModel.score(fv),
      actionFilter = actionFilter,
      leafCategories = leafCategories,
      dynamicBeamSize = _ => beamSize,
      branchingFactor = None,
      limitUnaryActions = false
    )).rejecting(_.sentence.length > rejectLength)

  val testStatsLogger = new ParseStatsCsvLogger(new File(logPath, s"test-stats.csv"))
  val testAmrFile = new File(logPath, s"test-amr.txt")
  val testAmrWriter = new PrintWriter(new BufferedWriter(new FileWriter(testAmrFile)))

  logger.info(s"Testing")
  jobs.Job(testExamples, "test")
    .parse(testDecoder, actualParallelism)
    .progressBar
    .logAmr(testAmrWriter, evaluationFunction)
    .logTopDerivationAsHtml(logPath)
    .evaluate(evaluationFunction)
    .printIterationStats
    .logStatsToCsv(testStatsLogger)
    .run()

  testStatsLogger.close()
  testAmrWriter.close()
}
