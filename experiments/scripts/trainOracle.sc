import $file.commonSettings
import java.io.{BufferedWriter, File, FileWriter, PrintWriter}

import gramr.experiments._
import gramr.graph.AdjacencyMeaningGraph
import gramr.jobs.Listener
import gramr.learn._
import gramr.parse.ActionPredicates
import gramr.parse.Implicits._
import gramr.text.Span
import gramr.util.{Logging, ParseStatsCsvLogger}
import gramr.experiments.ScriptSupport._
import commonSettings._
import gramr.learn.util.FeatureIndex
import gramr.jobs
import jobs.ops._

import scala.util.Random


@main
def main(
  train: String = "corpus/training/amr-release-1.0-training-proxy.txt",
  validate: String = "corpus/dev/amr-release-1.0-dev-proxy.txt",
  iterations: Int = 20,
  batchSize: Int = 64,
  trainSize: Option[Int] = None,
  valSize: Option[Int] = None,
  maxSentenceLength: Int = 40,
  testSentenceRejectLength: Int = 40,
  beamSize: Int = 10,
  maxModifieeDepth: Int = 0,
  learner: String = "adadelta",
  update: String = "early",
  oracleBeamSize: Int = 10,
  parallelism: Option[Int] = None,
  lexDir: String = ".",
  tagDir: String = ".",
  logDir: String = "logs/trainOracle_$DATE_$TIME",
  seed: Int = 1
): Unit = {

  val logPath: File = Logging.setupLogging(logDir)

  implicit val random: Random = new Random(seed)

  val alignments = "jamr+isi"
  val trainExamples = loadTrainData(train, alignments, tagDir, trainSize, maxSentenceLength)
  val valExamples = loadTestData(validate, tagDir, valSize, supertagFile = Some("val_tags_pred.txt"))


  // Load lexicon

  val lexicalChoice = loadLexicon[AdjacencyMeaningGraph](new File(lexDir), commonSettings.wildcardFillers)

  val actualParallelism = parallelism.getOrElse(  // default: allow at least 2GB per parsed example)
    Integer.min(
      Runtime.getRuntime.availableProcessors(),
      (Runtime.getRuntime.maxMemory() / (1024l * 1024l * 1024l * 2.0)).toInt))

  logger.info(s"Parsing with parallelism level $actualParallelism")


  // Build learner

  val learnerInstance = emptyLearner(learner)
  implicit val featureIndex: FeatureIndex = learnerInstance.trainModel.featureIndex
  implicit val fv: FeatureVector[MapFeatureVector] = MapFeatureVector.featureVectorLike
  implicit val bv: BaseVector[MapFeatureVector] = fv


  // Build oracle parser

  val combinatoryChoice = commonSettings.combinatoryChoice(maxModifieeDepth = maxModifieeDepth)

  val heuristicParser =
    gramr.parse.postprocess.resolveCorefs(
      gramr.parse.cky.heuristic[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](
        lexicalChoice = lexicalChoice,
        binaryChoice = combinatoryChoice,
        combinatorSystem = simpleCs,
        decisionFeatures = featureSet.derivationFeatures.extract,
        stateFeatures = featureSet.stateFeatures.extract,
        score = (c: Example[AdjacencyMeaningGraph]) => (mr: AdjacencyMeaningGraph, span: Span) =>
          ActionPredicates.GraphElementsCorrect.precision[AdjacencyMeaningGraph](mr, c.reference) * ActionPredicates.AlignmentsFulfilled.accuracy(mr, span, c),
        actionFilter = actionFilter,
        leafCategories = leafCategories,
        dynamicBeamSize = _ => oracleBeamSize,
        branchingFactor = None,
        limitUnaryActions = false
      )
    )

  val heuristicTrimmed = new gramr.parse.MemorisingParser(
    gramr.parse.postprocess.keepBestEvaluatedParse(heuristicParser, (mr: AdjacencyMeaningGraph, e: Example[AdjacencyMeaningGraph]) => evaluationFunction(mr, e).f1)
  )


  // Parse with oracle

  if(update.contains("early")) {
    val oracleStatsLogger = new ParseStatsCsvLogger(new File(logPath, "oracle-stats.csv"))
    val oracleAmrFile = new File(logPath, "oracle-amr.txt")
    val oracleAmrWriter = new PrintWriter(new BufferedWriter(new FileWriter(oracleAmrFile)))


    logger.info("Oracle parsing")
    jobs.Job(trainExamples, "oracle")
      .parse(heuristicTrimmed, actualParallelism, inOrder = false)
      .progressBar
      .logAmr(oracleAmrWriter, evaluationFunction)
      .logTopDerivationAsHtml(logPath)
      .logChartAsHtml(logPath)
      .evaluate(evaluationFunction)
      .printIterationStats
      .logStatsToCsv(oracleStatsLogger)
      .run()

    oracleStatsLogger.close()
    oracleAmrWriter.close()
  }

  // Build learned parser


  val updateStrategy = mkUpdateStrategy(update)

  val trainDecoder = gramr.parse.postprocess.resolveCorefs(
    gramr.parse.cky.learned[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](
      lexicalChoice = lexicalChoice,
      binaryChoice = combinatoryChoice,
      combinatorSystem = simpleCs,
      decisionFeatures = featureSet.derivationFeatures.extract,
      stateFeatures = featureSet.stateFeatures.extract,
      modelScore = fv => learnerInstance.trainModel.score(fv),
      actionFilter = actionFilter,
      leafCategories = leafCategories,
      dynamicBeamSize = _ => beamSize,
      branchingFactor = None,
      limitUnaryActions = false
    ))

  val valDecoder = gramr.parse.postprocess.resolveCorefs(
    gramr.parse.cky.learned[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](
      lexicalChoice = lexicalChoice,
      binaryChoice = combinatoryChoice,
      combinatorSystem = simpleCs,
      decisionFeatures = featureSet.derivationFeatures.extract,
      stateFeatures = featureSet.stateFeatures.extract,
      modelScore = fv => learnerInstance.testModel.score(fv),
      actionFilter = actionFilter,
      leafCategories = leafCategories,
      dynamicBeamSize = _ => beamSize,
      branchingFactor = None,
      limitUnaryActions = false
    )).rejecting(_.sentence.length > testSentenceRejectLength)


  for(i <- 1 to iterations) {
    val trainStatsLogger = new ParseStatsCsvLogger(new File(logPath, s"$i-train-stats.csv"))
    val trainAmrFile = new File(logPath, s"$i-train-amr.txt")
    val trainAmrWriter = new PrintWriter(new BufferedWriter(new FileWriter(trainAmrFile)))

    logger.info(s"Training iteration $i/$iterations")

    val shuffledExamples = random.shuffle(trainExamples)
    jobs.Job(shuffledExamples, s"$i-train")
      .dualParse(trainDecoder, heuristicTrimmed, actualParallelism, inOrder = false, firstName = "free", secondName = "oracle")
      .progressBar
      .addListener(Listener.evaluate(
        evaluationFunction,
        parseConsumers = Seq(Listener.logItemStatsToCsv("train", trainStatsLogger)),
        iterationConsumers = Seq(
          Listener.printIterationStats("train"),
          Listener.logIterationStatsToCsv("train", trainStatsLogger)
        )
      ))
      .logAmr(trainAmrWriter, evaluationFunction)
      .logTopDerivationAsHtml(logPath)
      .logChartAsHtml(logPath)
      .generateUpdates(updateStrategy)
      .applyTo(learnerInstance)
      .runBatched(batchSize)

    trainStatsLogger.close()
    trainAmrWriter.close()


    val modelFileName = s"$i-model.ser"
    logger.info(s"Saving model: $modelFileName")

    import java.io.FileOutputStream
    import java.io.ObjectOutputStream

    val modelOut: ObjectOutputStream = new ObjectOutputStream(new FileOutputStream(new File(logPath, modelFileName)))
    modelOut.writeObject(learnerInstance.trainModel)
    modelOut.close()


    val valStatsLogger = new ParseStatsCsvLogger(new File(logPath, s"$i-val-stats.csv"))
    val valAmrFile = new File(logPath, s"$i-val-amr.txt")
    val valAmrWriter = new PrintWriter(new BufferedWriter(new FileWriter(valAmrFile)))

    logger.info(s"Validating iteration $i/$iterations")
    jobs.Job(valExamples, s"$i-val")
      .parse(valDecoder, actualParallelism, inOrder = false)
      .progressBar
      .logAmr(valAmrWriter, evaluationFunction)
      .logTopDerivationAsHtml(logPath)
      .logChartAsHtml(logPath)
      .evaluate(evaluationFunction)
      .printIterationStats
      .logStatsToCsv(valStatsLogger)
      .run()

    valStatsLogger.close()
    valAmrWriter.close()
  }
}
