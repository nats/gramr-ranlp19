import $file.grammar.ccgaski
import $file.features.featureSet1

import java.io.{BufferedWriter, File, FileWriter, PrintWriter}

import gramr.amr.alignments.Combiners.intersectOrKeep
import gramr.amr.{AmrCorpusLoader, AttributeFileParser, LexicalGenerators}
import gramr.ccg._
import gramr.experiments.Example.ops._
import gramr.experiments._
import gramr.graph.AdjacencyMeaningGraph
import gramr.graph.AdjacencyMeaningGraph.ops._
import gramr.graph.Predicates._
import gramr.graph.lexemes.{LemmaFiller, PropbankFiller, QuotedFiller, WildcardFiller}
import gramr.jobs.AsyncJob
import gramr.jobs.ops._
import gramr.learn.MapFeatureVector
import gramr.parse.{ActionPredicate, ActionPredicates, LexicalChoice, PatternChoice, and, lexicalChoiceOps}
import gramr.text.Span
import gramr.util.{Logging, ParseStatsCsvLogger}

implicit val simpleCs: CombinatorSystem = new SimpleCombinatorSystem
import gramr.experiments.ScriptSupport._


// ------------------------------------------------------------------
// Grammar
// ------------------------------------------------------------------

val maxModifieeDepth = Some(2)
val rules = ccgaski.ccgAskiRules(maxModifieeDepth)

val wildcardFillers: scala.collection.immutable.Iterable[WildcardFiller] =
  scala.collection.immutable.Iterable(LemmaFiller(), QuotedFiller(), PropbankFiller())

val combinatoryChoice = new PatternChoice[AdjacencyMeaningGraph](simpleCs, rules)


// ------------------------------------------------------------------
// Features
// ------------------------------------------------------------------

val featureSet: FeatureSet[AdjacencyMeaningGraph, MapFeatureVector] = featureSet1.featureSet

@main
def main(
  train: String = "corpus/training/amr-release-1.0-training-proxy.txt",
  trainSize: Option[Int] = None,
  valSize: Option[Int] = None,
  maxSentenceLength: Int = 40,
  oracleBeamSize: Int = 10,
  parallelism: Option[Int] = None,
  lexDir: String = ".",
  logDir: String = "logs/oracleOnly_$DATE_$TIME"
): Unit = {

  val logPath: File = Logging.setupLogging(logDir)


  // Load data

  val trainSupertagFile = new File(lexDir, "train_tags_pred.txt")
  val trainSupertagDict = AttributeFileParser.parseFile(trainSupertagFile)

  val loader = AmrCorpusLoader[AdjacencyMeaningGraph, Example[AdjacencyMeaningGraph]]
    .useJamrAlignment("alignments-jamr", "alignments-jamr")
    .useIsiAlignment("alignments-isi", "alignments-isi")
    .combineAlignments("alignments-jamr", "alignments-isi", intersectOrKeep)
    .extendAttributes(trainSupertagDict)

  val trainExamples = AmrCorpusLoader.loadDataSet(loader, train, trainSize, Some(maxSentenceLength))
  // val valPath = "corpus/dev/amr-release-1.0-dev-proxy.txt"
  // val valExamples = loadDataSet(loader, valPath, trainSize, None)



  // Load lexicon

  val templateFile = new File(lexDir, "templates.txt")
  val lexemeFile = new File(lexDir, "lexemes.txt")

  val delexedLexicon = DelexedLexicon.load(
    templateFile=templateFile,
    lexemeFile=lexemeFile,
    wildcardFillers=wildcardFillers
  )

  val generatedLexicon = new GeneratedLexicon(
    "thing" -> LexicalGenerators.singleNode("thing", Set(SyntacticCategory("N"))),
    "emptymod" -> LexicalGenerators.identity,
    "numericDate" -> LexicalGenerators.numericDate,
    "dashDate" -> LexicalGenerators.dashDate,
    "americanSlashDate" -> LexicalGenerators.americanSlashDate
  )

  val delexedChoice: LexicalChoice[Example[AdjacencyMeaningGraph], AdjacencyMeaningGraph] = delexedLexicon.supertagLimitedLexicalChoice(
    extractTemplateTags = (e: Example[AdjacencyMeaningGraph]) => e.supertags("supertags-template-id")//,
    //extractLabelTags = Some((e: Example[AdjacencyMeaningGraph]) => e.supertags("supertags-lex-label"))
  )
  val generatedChoice: LexicalChoice[Example[AdjacencyMeaningGraph], AdjacencyMeaningGraph] = generatedLexicon.lexicalChoice
  val lexicalChoice = lexicalChoiceOps(delexedChoice) union generatedChoice


  // Build parser

  val trainActionFilter: ActionPredicate[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] = and(
    ActionPredicates.matchSyntacticArity,
    // limitNamedEntities(NamedEntities.proxyReductions),
    ActionPredicates.rootCategoryFilter(Set("S", "S[dcl]", "S[wq]", "S[q]", "NP", "N").map(SyntacticCategory.apply)),
    ActionPredicates.mrPredicate(mr => checkPolarityEdges(mr) && checkConstantDegrees(mr) && checkArgEdges(mr) && checkNumberedEdgesUnique(mr) && checkInstanceConcepts(mr))
  )

  val evaluationFunction = (mr: AdjacencyMeaningGraph, e: Example[AdjacencyMeaningGraph]) =>
    gramr.graph.evaluate.kernelSmatch(1, 4)(mr, e)

  def leafCategories(example: Example[AdjacencyMeaningGraph], span: Span): Iterable[SyntacticCategory] = {
    if(span.size == 1)
      if(span.start < example.ccgTags.size)
        example.ccgTags(span.start).map(_._1)
      else Iterable("N", "N/N").map(SyntacticCategory.apply)
    else Iterable.empty
  }

  val heuristicParser =
    gramr.parse.postprocess.resolveCorefs(
      gramr.parse.cky.heuristic[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](
        lexicalChoice = lexicalChoice,
        binaryChoice = combinatoryChoice,
        combinatorSystem = simpleCs,
        decisionFeatures = featureSet.derivationFeatures.extract,
        stateFeatures = featureSet.stateFeatures.extract,
        score = (c: Example[AdjacencyMeaningGraph]) => (mr: AdjacencyMeaningGraph, span: Span) =>
          ActionPredicates.GraphElementsCorrect.precision[AdjacencyMeaningGraph](mr, c.reference) * ActionPredicates.AlignmentsFulfilled.accuracy(mr, span, c),
        actionFilter = trainActionFilter,
        leafCategories = leafCategories,
        dynamicBeamSize = _ => oracleBeamSize,
        branchingFactor = None,
        limitUnaryActions = false
      )
    )

  val heuristicTrimmed = new gramr.parse.MemorisingParser(
    gramr.parse.postprocess.keepBestEvaluatedParse(heuristicParser, (mr: AdjacencyMeaningGraph, e: Example[AdjacencyMeaningGraph]) => evaluationFunction(mr, e).f1)
  )


  // Parse

  val actualParallelism = parallelism.getOrElse(  // default: allow at least 2GB per parsed example)
    Integer.min(
      Runtime.getRuntime.availableProcessors(),
      (Runtime.getRuntime.maxMemory() / (1024l * 1024l * 1024l * 2.0)).toInt))

  logger.info(s"Parsing with parallelism level $actualParallelism")

  val statsLogger = new ParseStatsCsvLogger(new File(logPath, "parseStats.csv"))
  val amrFile = new File(logPath, "amr.txt")
  val amrWriter = new PrintWriter(new BufferedWriter(new FileWriter(amrFile)))


  logger.info("Oracle parsing")
  AsyncJob("oracle", trainExamples)
    .parse(heuristicTrimmed, actualParallelism)
    .progressBar
    .oneLineEval(evaluationFunction)
    .logStatsToCsv(statsLogger, evaluationFunction)
    .logAmr(amrWriter)
    .run()

  statsLogger.close()
  amrWriter.close()
}
