#!/usr/bin/env bash
# Installs gramr.

# Prerequisites:
#   - git
#   - sbt

set -e

DEPSDIR=$1

echo Installing gramr
git clone https://gitlab.com/nats/gramr.git ${DEPSDIR}/gramr
cd ${DEPSDIR}/gramr
git checkout v272
sbt assembly
mv target/scala-2.12/gramr-assembly-272.jar gramr.jar
